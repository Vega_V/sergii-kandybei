package HelloWorld;
/**
 * Created by Vincent Vega on 18.04.2016.
 */

import java.util.Scanner;

public class HelloWorld{

    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
//Task1
        int res1 = 5 + 2;
        System.out.println("5 + 2 = " + res1);

        int res2 = 5 - 2;
        System.out.println("5 - 2 = " + res2);

        int res3 = 5 * 2;
        System.out.println("5 * 3 = " + res3);

        double a = 8;
        double b = 6;
        double res4 = a / b;
        System.out.println("a / b = " + res4);

        boolean res5 = a == b;
        System.out.println(res5);

        boolean t = true;

        boolean res6 = t && res5;
        System.out.println(res6);

        boolean res7 = t || res5;
        System.out.println(res7);

        t = !t;
        System.out.println(t);

        double c = 0;
        double d = 0;

        if (a > b){
            c = Math.sqrt(Math.abs(a));
            System.out.println("sqrt(|a|) = " + c);
        } else{
            d = Math.sqrt(Math.abs(b));
            System.out.println("sqrt(|b|) = " + d);
        }

//Task2

        byte a0 = 8;
        String a1 = Byte.toString(a0);
        System.out.println(a1 + " is a converted byte into string");
        String a2 = String.valueOf(a1);
        System.out.println(a2 + " is a converted string into byte");

        short b0 = 69;
        String b1 = Short.toString(b0);
        System.out.println(b1 + " is a converted short into string");
        String b2 = String.valueOf(b1);
        System.out.println(b2 + " is a converted string into short");

        int c0 = 7;
        String c1 = Integer.toString(c0);
        System.out.println(c1 + " is a converted integer into string");
        String c2 = String.valueOf(c1);
        System.out.println(c2 + " is a converted string into integer");

        long d0 = 40;
        String d1 = Long.toString(d0);
        System.out.println(d1 + " is a converted long into string");
        String d2 = String.valueOf(d1);
        System.out.println(d2 + " is a converted string into long");

        double e0 = 6.8;
        String e1 = Double.toString(e0);
        System.out.println(e1 + " is a converted double into string");
        String e2 = String.valueOf(e1);
        System.out.println(e2 + " is a converted string into double");

        float f0 = 0.9F;
        String f1 = Float.toString(f0);
        System.out.println(f1 + " is a converted float into string");
        String f2 = String.valueOf(f1);
        System.out.println(f2 + " is a converted string into float");

        char g0 = 'g';
        String g1 = Character.toString(g0);
        System.out.println(g1 + " is a converted char into string");
        String g2 = String.valueOf(g1);
        System.out.println(g2 + " is a converted string into char");

        boolean h0 = true;
        String h1 = Boolean.toString(h0);
        System.out.println(h1 + " is a converted boolean variable into string");
        String h2 = String.valueOf(h1);
        System.out.println(h2 + " is a converted string into boolean variable");

//Task3

        double l = 23.89;
        int intl = (int)l;
        System.out.println("double converted to integer " + intl);

        int y = 37;
        double dy = (double)y;
        System.out.println("integer converted to double " + dy);

//Task4
        System.out.print("Enter a first number: ");
        double numberOne = Double.valueOf(scanner.next()).doubleValue();

        System.out.print("Enter an operation: ");
        String operation = scanner.next();

        System.out.print("Enter a second number: ");
        double numberTwo = Double.valueOf(scanner.next()).doubleValue();

        System.out.println("Operation is: " + operation);

        double result = 0;

        if (operation.startsWith("+")){
            System.out.println("case +");
            result = numberOne + numberTwo;
        } else if (operation.startsWith("-")){
            System.out.println("case -");
            result = numberOne - numberTwo;
        } else if (operation.startsWith("*")){
            System.out.println("case -");
            result = numberOne * numberTwo;
        } else if (operation.startsWith("/")){
            System.out.println("case -");
            result = numberOne / numberTwo;
        } else {
            System.out.println("case else");
        }

         System.out.println("The result is: " + result);

        int intResult = (int)result;

        System.out.println("The Int result is: " + intResult);
    }
}







