package RaceCarDriver;

/**
 * Created by Vincent Vega on 24.04.2016.
 */
class Map {

    class MapItem{
    }

    class TurnMapItem extends MapItem{
    }

    class ForwardMapItem extends MapItem{
        int distance;

        ForwardMapItem(int d) {
            distance=d;
        }
    }

    MapItem[] generateSimpleMap() {
        int totalMapItemCount = RaceCarDriver.TOTAL_MAP_ITEMS_COUNT;
        MapItem objArr[] = new MapItem[totalMapItemCount*2];

        for (int i = 0; i < totalMapItemCount; i++ ){
            objArr[i*2] = new ForwardMapItem(RaceCarDriver.MAP_ITEM_DISTANCE);
            objArr[i*2 + 1] = new TurnMapItem();
        }

        return objArr;
    }
}
