package RaceCarDriver;

/**
 * Created by Vincent Vega on 24.04.2016.
 */
class Car {
    String name;
    double maxSpeed;
    double speed;
    double time;
    double acceleration;
    double mobility;

    Car(String name, double acceleration, double mobility, double maxSpeed) {
        this.name = name;
        this.acceleration = acceleration ;
        this.mobility = mobility;
        this.maxSpeed = maxSpeed * 1000 / 3600;

        this.speed = 0;
        this.time = 0;
    }

    void printStats(){
        System.out.printf("Car %s -> speed %f time %f maxSpeed %f \n", this.name, this.speed, this.time, this.maxSpeed);
    }

    void printTime() {
        int minutes = (int)(this.time / 60);
        double seconds = this.time - minutes * 60;
        System.out.printf("Car Time = %d minutes %f seconds \n", minutes, seconds);
    }


    void dispatchMapItem(Map.MapItem mapitem){
        if (mapitem instanceof Map.ForwardMapItem ){
            int dist = ((Map.ForwardMapItem) mapitem).distance;

            //System.out.printf("Car %s goes forward: %d km.\n", this.name, dist);
            this.makeForward(dist);
        }
        else if (mapitem instanceof Map.TurnMapItem ){
            //System.out.printf("Car %s turns.\n", this.name);
            this.makeTurn();
        }

        //this.printStats();
    }

    void makeForward(int distance){

        double newSpeed = Math.sqrt(2 * this.acceleration * distance + Math.pow(this.speed, 2));

        if (newSpeed > this.maxSpeed){

            double distance_accelerated = (Math.pow(this.maxSpeed, 2) - Math.pow(this.speed, 2)) / (2 * this.acceleration);
            this.time = this.time + 2 * distance_accelerated / (Math.sqrt(2 * this.acceleration * distance_accelerated + Math.pow(this.speed, 2)) + this.speed);

            //System.out.printf("Accelerated distance: %f km.\n", distance_accelerated);

            double distance_no_acceleration = distance - distance_accelerated;
            this.time = this.time + distance_no_acceleration / this.maxSpeed;

            //System.out.printf("Check1: %f km/h.\n", newSpeed);

            this.speed = this.maxSpeed;
        }
        else{
            this.speed = Math.sqrt(2 * this.acceleration * distance + Math.pow(this.speed, 2));
            this.time = this.time + 2 * distance / (Math.sqrt(2 * this.acceleration * distance + Math.pow(this.speed, 2)) + this.speed);
            //System.out.printf("Check2: %f km/h.\n", this.speed);
        }
    }

    void makeTurn(){
        this.speed = this.speed * this.mobility;
    }
}

