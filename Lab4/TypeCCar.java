package RaceCarDriver;

/**
 * Created by Vincent Vega on 25.04.2016.
 */
class TypeCCar extends Car{

    TypeCCar(String name, double acceleration, double mobility, double maxSpeed){
        super(name, acceleration, mobility, maxSpeed);
    }

    void makeTurn(){

        if (this.speed == this.maxSpeed ){

            this.maxSpeed = this.maxSpeed * 1.05;
            //System.out.printf("TypeCCar maxSpeedboost=%f added.\n", this.maxSpeed);
        }
    }
}

