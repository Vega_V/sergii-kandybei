package RaceCarDriver;

/**
 * Created by Vincent Vega on 24.04.2016.
 */
class TypeACar extends Car{

    TypeACar(String name, double acceleration, double mobility, double maxSpeed){
        super(name, acceleration, mobility, maxSpeed);
    }

    void makeTurn(){
        double mobilityBoost = 0;

        if (this.speed > (this.maxSpeed / 2)){

            mobilityBoost = (this.speed - this.maxSpeed / 2) / 200;
            this.mobility = this.mobility + mobilityBoost;
            //System.out.printf("TypeACar mobility boost=%f added.\n", mobilityBoost);
        }

        super.makeTurn();

        if (mobilityBoost > 0){
            this.mobility = this.mobility - mobilityBoost;
            //System.out.printf("TypeACar mobility boost=%f removed.\n", mobilityBoost);
        }
    }
}
