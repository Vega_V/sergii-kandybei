package RaceCarDriver;

/**
 * Created by Vincent Vega on 18.04.2016.
 */

public class RaceCarDriver{

    public static final int MAP_ITEM_DISTANCE=2000;
    public static final int TOTAL_MAP_ITEMS_COUNT=20;

    public static void main(String []args){
        Map map = new Map();

        Map.MapItem[] mapStructure = map.generateSimpleMap();

        TypeACar carA = new TypeACar("Snake Sanders", 2, 0.40, 30);

        TypeBCar carB = new TypeBCar("Jack Badlands", 2, 0.40, 30);

        TypeCCar carC = new TypeCCar("Roadkill Kelly", 2, 0.40, 30);

        for (int j = 0; j < mapStructure.length; j++ ){
            carA.dispatchMapItem(mapStructure[j]);
        }

        for (int j = 0; j < mapStructure.length; j++ ){
            carB.dispatchMapItem(mapStructure[j]);
        }

        for (int j = 0; j < mapStructure.length; j++ ){
            carC.dispatchMapItem(mapStructure[j]);
        }

        Car[] racingCars = new Car[3];
        racingCars[0] = carA;
        racingCars[1] = carB;
        racingCars[2] = carC;

        sortCarList(racingCars);
        printCarList(racingCars);

        }

        static Car[] sortCarList(Car[] carList) {
            boolean listSorted = false;

            while (!listSorted) {
                boolean order_changed = false;
                for (int j = 0; j < carList.length; j++) {
                    if (j < carList.length - 1) {
                        Car currentCar = carList[j];
                        Car nextCar = carList[j + 1];
                        if (currentCar.time > nextCar.time) {
                            carList[j] = nextCar;
                            carList[j + 1] = currentCar;
                            order_changed = true;
                        }
                    }
                }
                if (!order_changed) {
                    listSorted = true;
                }
            }
            return carList;
        }
           static void printCarList(Car[] carList){
                for (int j = 0; j < carList.length; j++ ){
                    System.out.printf("Car driver=%s.\n", ((Car) carList[j]).name);
                    ((Car) carList[j]).printTime();
                }
            }

        }




