package RaceCarDriver;

/**
 * Created by Vincent Vega on 25.04.2016.
 */
class TypeBCar extends Car{

    TypeBCar(String name, double acceleration, double mobility, double maxSpeed){
        super(name, acceleration, mobility, maxSpeed);
    }

    void makeForward(int distance){
        double accelerationBoost = 0;

        if (this.speed < (this.maxSpeed / 2)){

            accelerationBoost = this.acceleration;
            this.acceleration = this.acceleration + accelerationBoost;
            //System.out.printf("TypeBCar acceleration boost=%f added.\n", accelerationBoost);
        }

        super.makeForward(distance);

        if (accelerationBoost > 0){
            this.acceleration = this.acceleration - accelerationBoost;
            //System.out.printf("TypeBCar acceleration boost=%f removed.\n", accelerationBoost);
        }
    }
}

